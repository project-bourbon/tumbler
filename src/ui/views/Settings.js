import React from "react";
import CTAJoin from "ui/components/CTAJoin";

class Settings extends React.Component {
  render() {
    return (
      <div>
        <h1>Settings</h1>
        <CTAJoin />
      </div>
    );
  }
}

export default Settings;
