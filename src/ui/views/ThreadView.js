import React from "react";

class ThreadView extends React.Component {
  state = {
    thread: null
  };
  static getDerivedStateFromProps(props) {
    if (props.location.state && props.location.state.thread) {
      return {
        thread: props.location.state.thread
      };
    }
    return {
      thread: null
    };
  }
  render() {
    return <h1>{(this.state.thread && this.state.thread.title) || ""}</h1>;
  }
}

export default ThreadView;
