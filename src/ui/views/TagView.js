import React from "react";

class TagView extends React.Component {
  render() {
    return <h1>{this.props.match.params.tag}</h1>;
  }
}

export default TagView;
