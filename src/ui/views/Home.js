import React from "react";
import CTAJoin from "ui/components/CTAJoin";
import ThreadList from "ui/components/ThreadList";

const THREADS = [
  {
    title: "Howdy World!",
    text: null,
    tags: ["WhatsUpWithThat", "tagB"],
    link: "https://example.com",
    id: "LQKEK0M7Ob"
  },
  {
    title: "So Cool!",
    text: null,
    tags: ["yolo"],
    link: "https://example.com",
    id: "Vg6Xd7MGd3"
  },
  {
    title: "Much swag",
    text: null,
    tags: ["yolo", "YoloSwag"],
    link: "https://example.com",
    id: "PB9MbZM4j6"
  },
  {
    title: "123",
    text: null,
    tags: ["a", "c"],
    link: "https://example.com",
    id: "6KxJOLEjpl"
  },
  {
    title: "Can we fix it??",
    text: null,
    tags: ["bob", "builder"],
    link: "https://example.com",
    id: "89vJZbJdzY"
  },
  {
    title: "Text example!",
    text: "Howdy world! I'm some text! Look at me go!!!!!!!!!",
    tags: ["bob", "builder"],
    link: null,
    id: "v1jMgjEb2r"
  }
];

class Home extends React.Component {
  static async getInitialProps() {
    return { stuff: "whatever" };
  }
  render() {
    return (
      <div>
        <CTAJoin />
        <ThreadList threads={THREADS} />
      </div>
    );
  }
}

export default Home;
