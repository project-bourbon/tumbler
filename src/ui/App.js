import React from "react";
import { BrowserRouter } from "react-router-dom";
import { After } from "@jaredpalmer/after";

export default class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <After data={this.props.data} routes={this.props.routes} />
      </BrowserRouter>
    );
  }
}
