import styled from "react-emotion";

export default styled.button`
  background-color: #07f;
  color: white;
  padding: 0.75em 1em;
  border: none;
  border-radius: 9999px;
`;
