import React from "react";
import styled from "react-emotion";
import { Link } from "react-router-dom";

const ThreadWrapper = styled.li`
  display: grid;
  align-items: start;
  grid-gap: 0.25em;
`;

const TagContainer = styled.ul`
  display: grid;
  grid-auto-flow: column;
  grid-gap: 0.25em;
  grid-auto-columns: min-content;
  list-style: none;
  margin: 0;
  padding: 0;
`;

const Tag = styled.ul`
  background-color: rgba(0, 119, 255, 0.15);
  border-radius: 3px;
  margin: 0;
  padding: 0.25em;

  > a {
    color: black;
    text-decoration: none;
  }
`;

class ThreadListItem extends React.Component {
  render() {
    const { thread: t } = this.props;
    const hasLink = Boolean(t.link);
    const title = hasLink ? (
      <a href={t.link}>{t.title}</a>
    ) : (
      <Link
        to={{
          pathname: `/thread/${t.id}`,
          state: {
            thread: t
          }
        }}
      >
        {t.title}
      </Link>
    );
    return (
      <ThreadWrapper>
        {title}
        <TagContainer>
          {t.tags.map(tag => (
            <Tag key={tag}>
              <Link to={`/tag/${tag}`}>{tag}</Link>
            </Tag>
          ))}
        </TagContainer>
        <Link
          to={{
            pathname: `/thread/${t.id}`,
            state: {
              thread: t
            }
          }}
        >
          Comments
        </Link>
      </ThreadWrapper>
    );
  }
}

export default ThreadListItem;
