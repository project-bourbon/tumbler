import React from "react";
import styled from "react-emotion";
import Button from "ui/components/Button";
import { Link } from "react-router-dom";
import { APP_NAME } from "app-constants";

const LinkButton = styled(Button.withComponent(Link))`
  box-shadow: inset 0 -2px 0 rgba(0, 0, 0, 0.25);
`;

const Card = styled.div`
  display: flex;
  flex-direction: column;
  color: #222;
  justify-content: center;
  align-items: center;
  padding: 2em;
  box-shadow: inset 0 0 0 2px rgba(0, 0, 0, 0.25);
  border-radius: 0.5rem;

  h1 {
    margin: 0;
  }

  ${Button} {
    font-size: 1rem;
  }
`;

export default () => (
  <Card>
    <h1>Join the conversation on {APP_NAME}!</h1>
    <p>Share your interests and your ideas!</p>
    <LinkButton css={{ textDecoration: "none" }} to="/join">
      Join {APP_NAME}
    </LinkButton>
  </Card>
);
