import React from "react";
import { css } from "react-emotion";
import ThreadListItem from "ui/components/ThreadListItem";

const ulStyle = css`
  display: grid;
  grid-gap: 1em;
  align-items: start;
`;

class ThreadList extends React.Component {
  render() {
    return (
      <ul className={ulStyle}>
        {this.props.threads.map(thread => {
          return <ThreadListItem key={thread.id} thread={thread} />;
        })}
      </ul>
    );
  }
}

export default ThreadList;
