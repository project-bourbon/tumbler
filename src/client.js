import React from "react";
import { hydrate } from "react-dom";
import { ensureReady } from "@jaredpalmer/after";
import App from "ui/App";
import routes from "routes";
import { hydrate as hydrateStyles } from "emotion";
import "./global-styles.css";

const data = JSON.parse(
  document.getElementById("server-app-state").textContent
);

if (data.ids) hydrateStyles(data.ids);

ensureReady(routes).then(data =>
  hydrate(<App data={data} routes={routes} />, document.getElementById("root"))
);

if (module.hot) {
  module.hot.accept();
}
