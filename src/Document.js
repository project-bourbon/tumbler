import React from "react";
import { AfterRoot, AfterData } from "@jaredpalmer/after";
import { extractCritical } from "emotion-server";
import { APP_NAME } from "app-constants";

class Document extends React.Component {
  static async getInitialProps({ assets, data, renderPage }) {
    const page = await renderPage();
    const styles = extractCritical(page.html);
    return { styles, assets, data, ...page };
  }

  render() {
    const { helmet, assets, data, styles } = this.props;
    // get attributes from React Helmet
    const htmlAttrs = helmet.htmlAttributes.toComponent();
    const bodyAttrs = helmet.bodyAttributes.toComponent();

    const newData = {
      ...data,
      ids: styles.ids
    };

    return (
      <html {...htmlAttrs}>
        <head>
          <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
          <meta charSet="utf-8" />
          <title>{APP_NAME}</title>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          {helmet.title.toComponent()}
          {helmet.meta.toComponent()}
          {helmet.link.toComponent()}
          {assets.client.css && (
            <link rel="stylesheet" href={assets.client.css} />
          )}
          <style dangerouslySetInnerHTML={{ __html: styles.css }} />
        </head>
        <body {...bodyAttrs}>
          <AfterRoot />
          <AfterData data={newData} />
          <script
            type="text/javascript"
            src={assets.client.js}
            defer
            crossOrigin="anonymous"
          />
        </body>
      </html>
    );
  }
}

export default Document;
