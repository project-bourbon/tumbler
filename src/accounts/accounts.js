// Accounts
export async function createAccount() {}
export async function deleteAccount() {}
export async function editAccount() {}

// Authorization
export async function login() {}
export async function logout() {}
