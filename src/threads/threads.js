// Threads
export async function deleteThread() {}
export async function editThread() {}
export async function createThread() {}

// Comments
export async function createThreadComment() {}
export async function deleteThreadComment() {}
export async function editThreadComment() {}

// Reactions
export async function addThreadReaction() {}
export async function deleteThreadReaction() {}
export async function addCommentReaction() {}
export async function deleteCommentReaction() {}
