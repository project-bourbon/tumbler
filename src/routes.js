import React from "react";
import { asyncComponent } from "@jaredpalmer/after";

export default [
  {
    path: "/",
    exact: true,
    component: asyncComponent({
      loader: () => import("ui/views/Home")
    })
  },
  {
    path: "/join",
    component: () => <h1>Join</h1>
  },
  {
    path: "/login",
    component: () => <h1>login</h1>
  },
  {
    path: "/settings",
    component: asyncComponent({
      loader: () => import("ui/views/Settings")
    })
  },
  {
    path: "/tag/:tag",
    component: asyncComponent({
      loader: () => import("ui/views/TagView")
    })
  },
  {
    path: "/thread/:threadId",
    component: asyncComponent({
      loader: () => import("ui/views/ThreadView")
    })
  }
];
